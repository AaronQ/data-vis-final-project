from create_dash import create_map
from matplotlib import pyplot as plt
from wind_rose import create_windrose
from box_whisker import create_box
from box_whisker import sort_speed
from scatter_plot import create_scatter

# Create global variables
global fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_laken, bucket_lakenw, bucket_lakese
global bucket_lakesw, radio_buttons, sensor_buttons


def read_file(file_name):
    """
    reads in the file and puts the data into lists
    :param file_name: name of the input file
    :return: laken_meas: list of lists containing list of measurement values from LakeN sensor
             lakenw_meas: list of lists containing list of measurement values from LakeNW sensor
             lakese_meas: list of lists containing list of measurement values from LakeSE sensor
             lakesw_meas: list of lists containing list of measurement values from LakeSW sensor
    """
    with open(file_name, 'r') as f:
        variables = f.readline().rstrip().split(',')
        lakenw_meas = []
        laken_meas = []
        lakesw_meas = []
        lakese_meas = []
        for line in f:
            line_txt = line.rstrip().split(',')
            if line_txt[1] == 'LakeNW':
                lakenw_meas.append(line_txt)
            elif line_txt[1] == 'LakeN':
                laken_meas.append(line_txt)
            elif line_txt[1] == 'LakeSW':
                lakesw_meas.append(line_txt)
            elif line_txt[1] == 'LakeSE':
                lakese_meas.append(line_txt)
    # Transpose the data
    lakenw_meas = list(map(list, zip(*lakenw_meas)))
    laken_meas = list(map(list, zip(*laken_meas)))
    lakesw_meas = list(map(list, zip(*lakesw_meas)))
    lakese_meas = list(map(list, zip(*lakese_meas)))
    # Convert values of measurements to numbers for Northwest Sensor
    lakenw_meas[2] = [float(x) for x in lakenw_meas[2]]
    lakenw_meas[3] = [float(x) for x in lakenw_meas[3]]
    lakenw_meas[4] = [float(x) for x in lakenw_meas[4]]
    lakenw_meas[5] = [float(x) for x in lakenw_meas[5]]
    lakenw_meas[6] = [float(x) for x in lakenw_meas[6]]
    lakenw_meas[7] = [float(x) for x in lakenw_meas[7]]
    lakenw_meas[8] = [float(x) for x in lakenw_meas[8]]
    lakenw_meas[10] = [float(x) for x in lakenw_meas[10]]
    lakenw_meas[11] = [float(x) for x in lakenw_meas[11]]
    lakenw_meas[12] = [float(x) for x in lakenw_meas[12]]
    lakenw_meas[13] = [float(x) for x in lakenw_meas[13]]
    # Convert values of measurements to numbers for North Sensor
    laken_meas[2] = [float(x) for x in laken_meas[2]]
    laken_meas[3] = [float(x) for x in laken_meas[3]]
    laken_meas[4] = [float(x) for x in laken_meas[4]]
    laken_meas[5] = [float(x) for x in laken_meas[5]]
    laken_meas[6] = [float(x) for x in laken_meas[6]]
    laken_meas[7] = [float(x) for x in laken_meas[7]]
    laken_meas[8] = [float(x) for x in laken_meas[8]]
    laken_meas[10] = [float(x) for x in laken_meas[10]]
    laken_meas[11] = [float(x) for x in laken_meas[11]]
    laken_meas[12] = [float(x) for x in laken_meas[12]]
    laken_meas[13] = [float(x) for x in laken_meas[13]]
    # Convert values of measurements to numbers for Southwest Sensor
    lakesw_meas[2] = [float(x) for x in lakesw_meas[2]]
    lakesw_meas[3] = [float(x) for x in lakesw_meas[3]]
    lakesw_meas[4] = [float(x) for x in lakesw_meas[4]]
    lakesw_meas[5] = [float(x) for x in lakesw_meas[5]]
    lakesw_meas[6] = [float(x) for x in lakesw_meas[6]]
    lakesw_meas[7] = [float(x) for x in lakesw_meas[7]]
    lakesw_meas[8] = [float(x) for x in lakesw_meas[8]]
    lakesw_meas[10] = [float(x) for x in lakesw_meas[10]]
    lakesw_meas[11] = [float(x) for x in lakesw_meas[11]]
    lakesw_meas[12] = [float(x) for x in lakesw_meas[12]]
    lakesw_meas[13] = [float(x) for x in lakesw_meas[13]]
    # Convert values of measurements to numbers for Southeast Sensor
    lakese_meas[2] = [float(x) for x in lakese_meas[2]]
    lakese_meas[3] = [float(x) for x in lakese_meas[3]]
    lakese_meas[4] = [float(x) for x in lakese_meas[4]]
    lakese_meas[5] = [float(x) for x in lakese_meas[5]]
    lakese_meas[6] = [float(x) for x in lakese_meas[6]]
    lakese_meas[7] = [float(x) for x in lakese_meas[7]]
    lakese_meas[8] = [float(x) for x in lakese_meas[8]]
    lakese_meas[10] = [float(x) for x in lakese_meas[10]]
    lakese_meas[11] = [float(x) for x in lakese_meas[11]]
    lakese_meas[12] = [float(x) for x in lakese_meas[12]]
    lakese_meas[13] = [float(x) for x in lakese_meas[13]]
    return lakenw_meas, laken_meas, lakesw_meas, lakese_meas


def sort_date(lakenw_meas, laken_meas, lakesw_meas,  lakese_meas):
    """
    Takes sensor measurements, sorts based on datetime, then returns the sorted measurements
    :param laken_meas: list of lists containing list of measurement values from LakeN sensor
    :param lakenw_meas: list of lists containing list of measurement values from LakeNW sensor
    :param lakese_meas: list of lists containing list of measurement values from LakeSE sensor
    :param lakesw_meas: list of lists containing list of measurement values from LakeSW sensor
    :return: sorted_laken: list of lists containing list of measurement values from LakeN sensor sorted by datetime
             sorted_lakenw: list of lists containing list of measurement values from LakeNW sensor sorted by datetime
             sorted_lakese: list of lists containing list of measurement values from LakeSE sensor sorted by datetime
             sorted_lakesw: list of lists containing list of measurement values from LakeSW sensor sorted by datetime
    """
    sorted_lakenw = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
    sorted_laken = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
    sorted_lakesw = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]
    sorted_lakese = [[], [], [], [], [], [], [], [], [], [], [], [], [], []]

    sorted_lakenw[0] = sorted(lakenw_meas[0])
    sorted_lakenw[1] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[1]))]
    sorted_lakenw[2] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[2]))]
    sorted_lakenw[3] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[3]))]
    sorted_lakenw[4] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[4]))]
    sorted_lakenw[5] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[5]))]
    sorted_lakenw[6] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[6]))]
    sorted_lakenw[7] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[7]))]
    sorted_lakenw[8] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[8]))]
    sorted_lakenw[9] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[9]))]
    sorted_lakenw[10] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[10]))]
    sorted_lakenw[11] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[11]))]
    sorted_lakenw[12] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[12]))]
    sorted_lakenw[13] = [x for _, x in sorted(zip(lakenw_meas[0], lakenw_meas[13]))]

    sorted_laken[0] = sorted(laken_meas[0])
    sorted_laken[1] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[1]))]
    sorted_laken[2] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[2]))]
    sorted_laken[3] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[3]))]
    sorted_laken[4] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[4]))]
    sorted_laken[5] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[5]))]
    sorted_laken[6] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[6]))]
    sorted_laken[7] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[7]))]
    sorted_laken[8] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[8]))]
    sorted_laken[9] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[9]))]
    sorted_laken[10] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[10]))]
    sorted_laken[11] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[11]))]
    sorted_laken[12] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[12]))]
    sorted_laken[13] = [x for _, x in sorted(zip(laken_meas[0], laken_meas[13]))]

    sorted_lakesw[0] = sorted(lakesw_meas[0])
    sorted_lakesw[1] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[1]))]
    sorted_lakesw[2] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[2]))]
    sorted_lakesw[3] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[3]))]
    sorted_lakesw[4] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[4]))]
    sorted_lakesw[5] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[5]))]
    sorted_lakesw[6] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[6]))]
    sorted_lakesw[7] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[7]))]
    sorted_lakesw[8] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[8]))]
    sorted_lakesw[9] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[9]))]
    sorted_lakesw[10] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[10]))]
    sorted_lakesw[11] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[11]))]
    sorted_lakesw[12] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[12]))]
    sorted_lakesw[13] = [x for _, x in sorted(zip(lakesw_meas[0], lakesw_meas[13]))]

    sorted_lakese[0] = sorted(lakese_meas[0])
    sorted_lakese[1] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[1]))]
    sorted_lakese[2] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[2]))]
    sorted_lakese[3] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[3]))]
    sorted_lakese[4] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[4]))]
    sorted_lakese[5] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[5]))]
    sorted_lakese[6] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[6]))]
    sorted_lakese[7] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[7]))]
    sorted_lakese[8] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[8]))]
    sorted_lakese[9] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[9]))]
    sorted_lakese[10] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[10]))]
    sorted_lakese[11] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[11]))]
    sorted_lakese[12] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[12]))]
    sorted_lakese[13] = [x for _, x in sorted(zip(lakese_meas[0], lakese_meas[13]))]

    return sorted_lakenw, sorted_laken, sorted_lakesw, sorted_lakese


def graph_type(label):
    """

    :param label: label of the selected radiobutton
    """
    global fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_laken, bucket_lakenw, bucket_lakese
    global bucket_lakesw, radio_buttons, sensor_buttons
    all_buttons[0].activecolor='blue'
    if label == 'Wind Rose':
        create_windrose(fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_lakenw, bucket_laken,
                        bucket_lakesw, bucket_lakese,  all_buttons, wind_labels)
        all_buttons[0].activecolor = 'white'
    elif label == 'Box and Whisker':
        create_box(fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_lakenw, bucket_laken,
                        bucket_lakesw, bucket_lakese, all_buttons, wind_labels, [sorted_lakenw, sorted_laken, sorted_lakesw,
                                                                                 sorted_lakese])
        all_buttons[0].activecolor = 'white'
    elif label == 'Scatter':
        create_scatter(fig, labels, sensor_colors, all_buttons, [sorted_lakenw, sorted_laken, sorted_lakesw,
                                                                                 sorted_lakese])
        all_buttons[0].activecolor = 'white'


if __name__ == '__main__':
    """
    This script pulls in data from the output.csv file, sorts the data based on datetime, sorts the data based on wind 
    speeds, specifies the general direction colors, general directions, and sensor colors, then calls the dashboard
    function that creates the dashboard
    """
    # read data from file
    lakenw_meas, laken_meas, lakesw_meas, lakese_meas = read_file('output.csv')

    sorted_lakenw, sorted_laken, sorted_lakesw, sorted_lakese = sort_date(lakenw_meas, laken_meas,
                                                                          lakesw_meas, lakese_meas)

    bucket_lakenw, bucket_laken, bucket_lakesw, bucket_lakese = sort_speed(sorted_lakenw, sorted_laken,
                                                                           sorted_lakesw, sorted_lakese)

    vector_color = ['#084594', '#ae1082', '#0082a3', '#feb24c', '#f37625', '#4daf4a',
                    '#a7b43d', '#e41a1c']
    gen_dir_types = ['North', 'Northeast', 'East', 'Southeast', 'South', 'Southwest', 'West', 'Northwest']
    labels = ['LakeNW Sensor', 'LakeN Sensor', 'LakeSW Sensor', 'LakeSE Sensor']
    sensor_colors = ['#084594', '#ae1082', '#0082a3', '#feb24c']
    fig, all_buttons, wind_labels = create_map(sensor_colors, labels, gen_dir_types)
    all_buttons[0].on_clicked(graph_type)
    plt.show()
