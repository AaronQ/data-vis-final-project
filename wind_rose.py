from matplotlib import pyplot as plt


def create_windrose(fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_lakenw, bucket_laken,
                        bucket_lakesw, bucket_lakese, all_buttons, wind_labels):
    axes = fig.axes
    wind_gs = None
    sensor_buttons = all_buttons[1]
    main_button = all_buttons[2]
    check_boxes = all_buttons[3]
    wr_plots = []
    # loop through the created axes and isolate the ones needed to create the graphs
    for i in range(0, len(axes), 1):
        axes[i].set_visible(not axes[i].get_visible()) if i not in [4, 9, 10, 11, 13, 14, 19, 20] else None
        wind_gs = axes[i].get_gridspec() if i == 1 else wind_gs
        wr_plots.append(axes[i]) if i in [5, 6, 7, 8] else None
    count_labels = [labels[i] + ' - Reading Count: ' + str(len(bucket_lakenw[0][8] + bucket_lakenw[1][8] + bucket_lakenw[2][8] + bucket_lakenw[3][8])) for i in range(0, len(labels), 1)]
    wr_plots[0].title.set_text(count_labels[0])
    wr_plots[0].set_facecolor('#D9D9D9')
    wr_plots[1].title.set_text(count_labels[1])
    wr_plots[1].set_facecolor('#D9D9D9')
    wr_plots[2].title.set_text(count_labels[2])
    wr_plots[2].set_facecolor('#D9D9D9')
    wr_plots[3].title.set_text(count_labels[3])
    wr_plots[3].set_facecolor('#D9D9D9')
    wr_plots[0].bar(bucket_lakenw[0][8] + bucket_lakenw[1][8] + bucket_lakenw[2][8] + bucket_lakenw[3][8],
                    bucket_lakenw[0][6] + bucket_lakenw[1][6] + bucket_lakenw[2][6] + bucket_lakenw[3][6],
                    normed=True, opening=0.8, edgecolor='white')
    wr_plots[0].legend(loc=(-.4, 0))
    wr_plots[0].yaxis.set_major_formatter('{x:1.0f}%')
    wr_plots[1].bar(bucket_laken[0][8] + bucket_laken[1][8] + bucket_laken[2][8] + bucket_laken[3][8],
                    bucket_laken[0][6] + bucket_laken[1][6] + bucket_laken[2][6] + bucket_laken[3][6],
                    normed=True, opening=0.8, edgecolor='white')
    wr_plots[1].legend(loc=(-.4, 0))
    wr_plots[1].yaxis.set_major_formatter('{x:1.0f}%')
    wr_plots[2].bar(bucket_lakesw[0][8] + bucket_lakesw[1][8] + bucket_lakesw[2][8] + bucket_lakesw[3][8],
                    bucket_lakesw[0][6] + bucket_lakesw[1][6] + bucket_lakesw[2][6] + bucket_lakesw[3][6],
                    normed=True, opening=0.8, edgecolor='white')
    wr_plots[2].legend(loc=(-.4, 0))
    wr_plots[2].yaxis.set_major_formatter('{x:1.0f}%')
    wr_plots[3].bar(bucket_lakese[0][8] + bucket_lakese[1][8] + bucket_lakese[2][8] + bucket_lakese[3][8],
                    bucket_lakese[0][6] + bucket_lakese[1][6] + bucket_lakese[2][6] + bucket_lakese[3][6],
                    normed=True, opening=0.8, edgecolor='white')
    wr_plots[3].legend(loc=(-.4, 0))
    wr_plots[3].yaxis.set_major_formatter('{x:1.0f}%')
    fig.subplots_adjust(left=.05, bottom=0.035, right=.95, top=0.94, wspace=0, hspace=0)

    def checked(label):
        check_boxes.eventson = False
        index = wind_labels.index(label)
        is_checked = check_boxes.get_status()
        wr_plots[0].clear()
        wr_plots[1].clear()
        wr_plots[2].clear()
        wr_plots[3].clear()
        index = 0 if sum(is_checked) == 4 else index
        index = 0 if sum(is_checked) == 0 else index
        leg_loc = -.2 if sum(selected_sensors) == 1 else -.4
        if index != 0:
            check_boxes.set_active(0) if is_checked[0] else None
            lakenw_data = [[], []]
            laken_data = [[], []]
            lakesw_data = [[], []]
            lakese_data = [[], []]
            for i in range(1, len(is_checked), 1):
                if is_checked[i]:
                    lakenw_data[0] = lakenw_data[0] + bucket_lakenw[i - 1][8]
                    lakenw_data[1] = lakenw_data[1] + bucket_lakenw[i - 1][6]
                    laken_data[0] = laken_data[0] + bucket_laken[i-1][8]
                    laken_data[1] = laken_data[1] + bucket_laken[i-1][6]
                    lakesw_data[0] = lakesw_data[0] + bucket_lakesw[i - 1][8]
                    lakesw_data[1] = lakesw_data[1] + bucket_lakesw[i - 1][6]
                    lakese_data[0] = lakese_data[0] + bucket_lakese[i-1][8]
                    lakese_data[1] = lakese_data[1] + bucket_lakese[i-1][6]
            wr_plots[0].bar(lakenw_data[0], lakenw_data[1], normed=True, opening=0.8, edgecolor='white')
            wr_plots[0].legend(loc=(leg_loc, 0))
            wr_plots[0].yaxis.set_major_formatter('{x:1.0f}%')
            nwcount_labels = labels[0] + ' - Reading Count: ' + str(len(lakenw_data[0]))
            wr_plots[0].title.set_text(nwcount_labels)
            wr_plots[1].bar(laken_data[0], laken_data[1], normed=True, opening=0.8, edgecolor='white')
            wr_plots[1].legend(loc=(leg_loc, 0))
            wr_plots[1].yaxis.set_major_formatter('{x:1.0f}%')
            ncount_labels = labels[1] + ' - Reading Count: ' + str(len(laken_data[0]))
            wr_plots[1].title.set_text(ncount_labels)
            wr_plots[2].bar(lakesw_data[0], lakesw_data[1], normed=True, opening=0.8, edgecolor='white')
            wr_plots[2].legend(loc=(leg_loc, 0))
            wr_plots[2].yaxis.set_major_formatter('{x:1.0f}%')
            swcount_labels = labels[2] + ' - Reading Count: ' + str(len(lakesw_data[0]))
            wr_plots[2].title.set_text(swcount_labels)
            wr_plots[3].bar(lakese_data[0], lakese_data[1], normed=True, opening=0.8, edgecolor='white')
            wr_plots[3].legend(loc=(leg_loc, 0))
            wr_plots[3].yaxis.set_major_formatter('{x:1.0f}%')
            secount_labels = labels[3] + ' - Reading Count: ' + str(len(lakese_data[0]))
            wr_plots[3].title.set_text(secount_labels)
        elif index == 0:
            check_boxes.set_active(0) if not is_checked[0] else None
            [check_boxes.set_active(i) for i in range(1, len(is_checked), 1) if is_checked[i]]
            wr_plots[0].bar(bucket_lakenw[0][8] + bucket_lakenw[1][8] + bucket_lakenw[2][8] + bucket_lakenw[3][8],
                            bucket_lakenw[0][6] + bucket_lakenw[1][6] + bucket_lakenw[2][6] + bucket_lakenw[3][6],
                            normed=True, opening=0.8, edgecolor='white')
            wr_plots[0].legend(loc=(leg_loc, 0))
            wr_plots[0].title.set_text(count_labels[0])
            wr_plots[1].bar(bucket_laken[0][8] + bucket_laken[1][8] + bucket_laken[2][8] + bucket_laken[3][8],
                            bucket_laken[0][6] + bucket_laken[1][6] + bucket_laken[2][6] + bucket_laken[3][6],
                            normed=True, opening=0.8, edgecolor='white')
            wr_plots[1].legend(loc=(leg_loc, 0))
            wr_plots[1].title.set_text(count_labels[1])
            wr_plots[2].bar(bucket_lakesw[0][8] + bucket_lakesw[1][8] + bucket_lakesw[2][8] + bucket_lakesw[3][8],
                            bucket_lakesw[0][6] + bucket_lakesw[1][6] + bucket_lakesw[2][6] + bucket_lakesw[3][6],
                            normed=True, opening=0.8, edgecolor='white')
            wr_plots[2].legend(loc=(leg_loc, 0))
            wr_plots[2].title.set_text(count_labels[2])
            wr_plots[3].bar(bucket_lakese[0][8] + bucket_lakese[1][8] + bucket_lakese[2][8] + bucket_lakese[3][8],
                            bucket_lakese[0][6] + bucket_lakese[1][6] + bucket_lakese[2][6] + bucket_lakese[3][6],
                            normed=True, opening=0.8, edgecolor='white')
            wr_plots[3].legend(loc=(leg_loc, 0))
            wr_plots[3].title.set_text(count_labels[3])
        check_boxes.eventson = True
        fig.canvas.draw()


    # Create array of selected sensors
    selected_sensors = [False, False, False, False]

    def sensor_clicked(event, index):
        """Hide or Show sensor plots
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        # Change the status of the selected sensor
        selected_sensors[index] = not selected_sensors[index]
        # Change color of selected button
        if selected_sensors[index]:
            sensor_buttons[index].color = 'gray'
            sensor_buttons[index].label.set_color('black')
        else:
            sensor_buttons[index].color = 'white'
            sensor_buttons[index].label.set_color(sensor_colors[index])
        # Change visibility of plots
        if all(selected_sensors) or not any(selected_sensors):
            for i in range(0, 4, 1):
                sensor_buttons[i].color = 'white'
                sensor_buttons[i].label.set_color(sensor_colors[i])
                selected_sensors[i] = False
                wr_plots[i].set_visible(True)
            wr_plots[0].set_position(wind_gs[0:13, 10:20].get_position(fig))
            wr_plots[0].legend(loc=(-.4, 0))
            wr_plots[1].set_position(wind_gs[0:13, 21:31].get_position(fig))
            wr_plots[1].legend(loc=(-.4, 0))
            wr_plots[2].set_position(wind_gs[17:30, 21:31].get_position(fig))
            wr_plots[2].legend(loc=(-.4, 0))
            wr_plots[3].set_position(wind_gs[17:30, 10:20].get_position(fig))
            wr_plots[3].legend(loc=(-.4, 0))
        else:
            for i in range(0, 4, 1):
                if not selected_sensors[i]:
                    wr_plots[i].set_visible(False)
                else:
                    if selected_sensors[i] and sum(selected_sensors) == 1:
                        wr_plots[i].set_position(wind_gs[0:30, 10:31].get_position(fig))
                        wr_plots[i].legend(loc=(-.2, 0))
                    elif selected_sensors[i] and sum(selected_sensors) == 2:
                        if sum(selected_sensors[0:i+1]) == 1:
                            wr_plots[i].set_position(wind_gs[9:22, 10:20].get_position(fig))
                            wr_plots[i].legend(loc=(-.4, 0))
                        elif sum(selected_sensors[0:i+1]) == 2:
                            wr_plots[i].set_position(wind_gs[9:22, 20:31].get_position(fig))
                            wr_plots[i].legend(loc=(-.4, 0))
                    elif selected_sensors[i] and sum(selected_sensors) == 3:
                        if sum(selected_sensors[0:i+1]) == 1:
                            wr_plots[i].set_position(wind_gs[0:13, 10:20].get_position(fig))
                            wr_plots[i].legend(loc=(-.4, 0))
                        elif sum(selected_sensors[0:i+1]) == 2:
                            wr_plots[i].set_position(wind_gs[0:13, 20:31].get_position(fig))
                            wr_plots[i].legend(loc=(-.4, 0))
                        elif sum(selected_sensors[0:i+1]) == 3:
                            wr_plots[i].set_position(wind_gs[17:30, 10:20].get_position(fig))
                            wr_plots[i].legend(loc=(-.4, 0))
                    wr_plots[i].set_visible(True)

    def main_clicked(event):
        # Clear selections
        checked('All (default)')
        if any(selected_sensors):
            [sensor_clicked(None, i) for i in range(0, len(selected_sensors), 1) if selected_sensors[i]]
        for i in range(0, len(axes), 1):
            if i in [5, 6, 7, 8]:
                axes[i].set_visible(False) if axes[i].get_visible() else None
            else:
                axes[i].set_visible(not axes[i].get_visible()) if i not in [4, 9, 10, 11, 13, 14, 19, 20] else None
        fig.subplots_adjust(left=0, bottom=0.05, top=1, right=1, wspace=1, hspace=1)
        # disconnect button events
        main_button.disconnect(mb_cid)
        check_boxes.disconnect(cb_cid)
        [sensor_buttons[i].disconnect(sb_cid[i]) for i in range(0, len(sensor_buttons), 1)]
        return

    mb_cid = main_button.on_clicked(main_clicked)
    cb_cid = check_boxes.on_clicked(checked)
    sb_cid = [sensor_buttons[0].on_clicked(lambda x: sensor_clicked(x, 0))]
    sb_cid.append(sensor_buttons[1].on_clicked(lambda x: sensor_clicked(x, 1)))
    sb_cid.append(sensor_buttons[2].on_clicked(lambda x: sensor_clicked(x, 2)))
    sb_cid.append(sensor_buttons[3].on_clicked(lambda x: sensor_clicked(x, 3)))

    plt.draw()
