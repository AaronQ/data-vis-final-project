from matplotlib import pyplot as plt



def create_box(fig, vector_color, gen_dir_types, labels, sensor_colors, bucket_lakenw, bucket_laken,
                        bucket_lakesw, bucket_lakese, all_buttons, wind_labels, total_data):
    axes = fig.axes
    total_filtered = [bucket_lakenw, bucket_laken, bucket_lakesw, bucket_lakese]
    box_plots = None
    wdax = None
    for i in range(0, len(axes), 1):
        axes[i].set_visible(not axes[i].get_visible()) if i not in [4, 5, 6, 7, 8, 10, 11, 13, 14, 19, 20] else None
        box_plots = axes[i] if i == 9 else box_plots
        wdax = axes[i] if i == 4 else wdax
    box_plots.title.set_text('Wind Speeds')
    sensor_buttons = all_buttons[1]
    main_button = all_buttons[2]
    check_boxes = all_buttons[3]
    wind_dir_buttons = all_buttons[4]
    fig.subplots_adjust(left=.05, bottom=0.035, right=.95, top=0.94, wspace=0, hspace=0)
    sensor_data = [bucket_lakenw[0][6] + bucket_lakenw[1][6] + bucket_lakenw[2][6] + bucket_lakenw[3][6],
                   bucket_laken[0][6] + bucket_laken[1][6] + bucket_laken[2][6] + bucket_laken[3][6],
                   bucket_lakesw[0][6] + bucket_lakesw[1][6] + bucket_lakesw[2][6] + bucket_lakesw[3][6],
                   bucket_lakese[0][6] + bucket_lakese[1][6] + bucket_lakese[2][6] + bucket_lakese[3][6]]
    count_labels = [labels[i] + '\nReading Count: ' + str(len(sensor_data[i])) for i in range(0, len(labels), 1)]
    box_temp = box_plots.boxplot(sensor_data, labels=count_labels, patch_artist=True, flierprops={"markersize":"3"},
                                 medianprops={"color": "white"})
    [patch.set_facecolor(color) for patch, color in zip(box_temp['boxes'], sensor_colors)]
    box_plots.set_facecolor('#D9D9D9')
    box_plots.tick_params(axis='x', which='major', labelsize=12)
    box_plots.yaxis.set_major_formatter('{x:1.0f} mph')


    # Create array of selected sensors
    selected_sensors = [False, False, False, False]
    selected_bucket = [True, False, False, False, False]
    selected_direction = 'All (Default)'

    def update_plot():
        nonlocal selected_bucket, selected_sensors, selected_direction, total_filtered
        box_plots.clear()
        lakenw_speed = []
        laken_speed = []
        lakesw_speed = []
        lakese_speed = []
        if selected_direction == 'All (Default)' and selected_bucket[0]:
            box_temp = box_plots.boxplot(sensor_data, labels=count_labels, patch_artist=True, flierprops={"markersize":"3"},
                                 medianprops={"color": "white"})
            [patch.set_facecolor(color) for patch, color in zip(box_temp['boxes'], sensor_colors)]
            box_plots.yaxis.set_major_formatter('{x:1.0f} mph')
            box_plots.set_facecolor('#D9D9D9')
            filtered_speed = sensor_data
        elif selected_direction == 'All (Default)' and not selected_bucket[0]:
            for i in range(1, len(selected_bucket), 1):
                if selected_bucket[i]:
                    lakenw_speed = lakenw_speed + bucket_lakenw[i - 1][6]
                    laken_speed = laken_speed + bucket_laken[i - 1][6]
                    lakesw_speed = lakesw_speed + bucket_lakesw[i - 1][6]
                    lakese_speed = lakese_speed + bucket_lakese[i - 1][6]
            filtered_speed = [lakenw_speed, laken_speed, lakesw_speed, lakese_speed]
        else:
            sensor_index = selected_sensors.index(True)
            filtered_speed = [lakenw_speed, laken_speed, lakesw_speed, lakese_speed]
            for i in range(1, len(selected_bucket), 1):
                for j in range(0, len(total_filtered[sensor_index][i-1][9]), 1):
                    if total_filtered[sensor_index][i-1][9][j] == selected_direction and any(
                            [selected_bucket[i], selected_bucket[0]]):
                        datetime = total_filtered[sensor_index][i-1][0][j]
                        filtered_speed[sensor_index].append(total_filtered[sensor_index][i-1][6][j])
                        filtered_speed[0].append(
                            total_data[0][6][total_data[0][0].index(datetime)]) if sensor_index != 0 else None
                        filtered_speed[1].append(
                            total_data[1][6][total_data[1][0].index(datetime)]) if sensor_index != 1 else None
                        filtered_speed[2].append(
                            total_data[2][6][total_data[2][0].index(datetime)]) if sensor_index != 2 else None
                        filtered_speed[3].append(
                            total_data[3][6][total_data[3][0].index(datetime)]) if sensor_index != 3 else None
        filtered_labels = [labels[i] + '\nReading Count: ' + str(len(filtered_speed[i])) for i in
                           range(0, len(labels), 1)]
        box_temp = box_plots.boxplot(filtered_speed, labels=filtered_labels, patch_artist=True, flierprops={"markersize":"3"},
                                 medianprops={"color": "white"})
        [patch.set_facecolor(color) for patch, color in zip(box_temp['boxes'], sensor_colors)]
        box_plots.yaxis.set_major_formatter('{x:1.0f} mph')
        box_plots.set_facecolor('#D9D9D9')
        fig.canvas.draw()

    def checked(label):
        nonlocal selected_bucket, selected_sensors, selected_direction
        # Initialize variables for use
        check_boxes.eventson = False
        index = wind_labels.index(label)
        is_checked = check_boxes.get_status()
        index = 0 if sum(is_checked) == 4 else index
        index = 0 if sum(is_checked) == 0 else index
        if index != 0:
            check_boxes.set_active(0) if is_checked[0] else None
            selected_bucket[index] = is_checked[index]
            selected_bucket[0] = False
        elif index == 0:
            check_boxes.set_active(0) if not is_checked[0] else None
            [check_boxes.set_active(i) for i in range(1, len(is_checked), 1) if is_checked[i]]
            selected_bucket = [True, False, False, False, False]
        check_boxes.eventson = True
        update_plot()


    def sensor_clicked(event, index):
        """Hide or Show sensor plots
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        nonlocal selected_direction
        # Change the status of the selected sensor and show the radiobuttons
        selected_sensors[index] = not selected_sensors[index]
        wdax.set_visible(True) if selected_sensors[index] else wdax.set_visible(False)
        wdax.set_title('Choose general wind\ndirection of ' + labels[index] + ':', loc='left', size='small')
        # Change color of selected button
        for i in range(0, 4, 1):
            selected_sensors[i] = not selected_sensors[i] if all([i != index,  selected_sensors[i]]) else selected_sensors[i]
            if not any(selected_sensors):
                sensor_buttons[i].color = 'white'
                sensor_buttons[i].label.set_color(sensor_colors[i])
                selected_sensors[i] = False
                selected_direction = 'All (Default)'
                wind_dir_buttons.set_active(0)
            elif not selected_sensors[i]:
                sensor_buttons[i].label.set_color('gray')
                sensor_buttons[i].label.set_fontstyle('italic')
                sensor_buttons[i].color = 'white'
            else:
                sensor_buttons[i].label.set_color(sensor_colors[i])
                sensor_buttons[i].color = 'gray'
        if selected_direction != 'All (Default)':
            update_plot()


    def wind_direction(label):
        nonlocal selected_bucket, selected_sensors, selected_direction
        selected_direction = label
        update_plot()

    def main_clicked(event):
        for i in range(0, len(axes), 1):
            if i in [5, 6, 7, 8]:
                axes[i].set_visible(False) if axes[i].get_visible() else None
            else:
                axes[i].set_visible(not axes[i].get_visible()) if i not in [4, 10, 11, 13, 14, 19, 20] else None
        fig.subplots_adjust(left=0, bottom=0.05, top=1, right=1, wspace=1, hspace=1)
        # disconnect button events
        main_button.disconnect(mb_cid)
        check_boxes.disconnect(cb_cid)
        wind_dir_buttons.disconnect(wd_cid)
        [sensor_buttons[i].disconnect(sb_cid[i]) for i in range(0, len(sensor_buttons), 1)]
        # Clear selections
        checked('All (default)')
        if any(selected_sensors):
            [sensor_clicked(None, i) for i in range(0, len(selected_sensors), 1) if selected_sensors[i]]
        return

    mb_cid = main_button.on_clicked(main_clicked)
    cb_cid = check_boxes.on_clicked(checked)
    sb_cid = [sensor_buttons[0].on_clicked(lambda x: sensor_clicked(x, 0))]
    sb_cid.append(sensor_buttons[1].on_clicked(lambda x: sensor_clicked(x, 1)))
    sb_cid.append(sensor_buttons[2].on_clicked(lambda x: sensor_clicked(x, 2)))
    sb_cid.append(sensor_buttons[3].on_clicked(lambda x: sensor_clicked(x, 3)))
    wd_cid = wind_dir_buttons.on_clicked(wind_direction)
    plt.draw()


def sort_speed(sorted_laken, sorted_lakenw, sorted_lakese, sorted_lakesw):
    """
    Takes date sorted sensor measurements, puts reading into bucket based on wind speed, then returns the wind speed
    buckets.
    :param sorted_laken: list of lists containing list of measurement values from LakeN sensor sorted by datetime
    :param sorted_lakenw: list of lists containing list of measurement values from LakeNW sensor sorted by datetime
    :param sorted_lakese: list of lists containing list of measurement values from LakeSE sensor sorted by datetime
    :param sorted_lakesw: list of lists containing list of measurement values from LakeSW sensor sorted by datetime
    :return: bucket_laken: list of list of lists containing list of measurement values from LakeN Sensor sorted by
                           datetime and divided by wind speed
             bucket_lakenw:list of list of lists containing list of measurement values from LakeN Sensor sorted by
                           datetime and divided by wind speed
             bucket_lakese:list of list of lists containing list of measurement values from LakeN Sensor sorted by
                           datetime and divided by wind speed
             bucket_lakesw:list of list of lists containing list of measurement values from LakeN Sensor sorted by
                           datetime and divided by wind speed
    """
    # initialize bucket variables
    bucket_laken = [[], [], [], []]
    bucket_lakenw = [[], [], [], []]
    bucket_lakese = [[], [], [], []]
    bucket_lakesw = [[], [], [], []]

    if (len(sorted_laken[0]) + len(sorted_lakenw[0]) + len(sorted_lakesw[0]) + len(sorted_lakese[0]))/4 != \
            len(sorted_laken[0]):
        print("Different number of sensor readings per sensor exist.")

    # loop through values of speed for each sensor, add reading to bucket for corresponding wind speed bucket
    tempn = list(zip(*sorted_laken))
    tempnw = list(zip(*sorted_lakenw))
    tempse = list(zip(*sorted_lakese))
    tempsw = list(zip(*sorted_lakesw))

    for i in range(0, len(sorted_laken[0]), 1):
        if sorted_laken[6][i] == 0:
            if len(bucket_laken[0]) == 0:
                to_add = [[j] for j in tempn[i]]
                bucket_laken[0] = to_add
            else:
                [bucket_laken[0][j].append(tempn[i][j]) for j in range(0, len(tempn[i]), 1)]
        elif 0 < sorted_laken[6][i] <= 5:
            if len(bucket_laken[1]) == 0:
                to_add = [[j] for j in tempn[i]]
                bucket_laken[1] = to_add
            else:
                [bucket_laken[1][j].append(tempn[i][j]) for j in range(0, len(tempn[i]), 1)]
        elif 5 < sorted_laken[6][i] <= 10:
            if len(bucket_laken[2]) == 0:
                to_add = [[j] for j in tempn[i]]
                bucket_laken[2] = to_add
            else:
                [bucket_laken[2][j].append(tempn[i][j]) for j in range(0, len(tempn[i]), 1)]
        else:
            if len(bucket_laken[3]) == 0:
                to_add = [[j] for j in tempn[i]]
                bucket_laken[3] = to_add
            else:
                [bucket_laken[3][j].append(tempn[i][j]) for j in range(0, len(tempn[i]), 1)]

        if sorted_lakenw[6][i] == 0:
            if len(bucket_lakenw[0]) == 0:
                to_add = [[j] for j in tempnw[i]]
                bucket_lakenw[0] = to_add
            else:
                [bucket_lakenw[0][j].append(tempnw[i][j]) for j in range(0, len(tempnw[i]), 1)]
        elif 0 < sorted_lakenw[6][i] <= 5:
            if len(bucket_lakenw[1]) == 0:
                to_add = [[j] for j in tempnw[i]]
                bucket_lakenw[1] = to_add
            else:
                [bucket_lakenw[1][j].append(tempnw[i][j]) for j in range(0, len(tempnw[i]), 1)]
        elif 5 < sorted_lakenw[6][i] <= 10:
            if len(bucket_lakenw[2]) == 0:
                to_add = [[j] for j in tempnw[i]]
                bucket_lakenw[2] = to_add
            else:
                [bucket_lakenw[2][j].append(tempnw[i][j]) for j in range(0, len(tempnw[i]), 1)]
        else:
            if len(bucket_lakenw[3]) == 0:
                to_add = [[j] for j in tempnw[i]]
                bucket_lakenw[3] = to_add
            else:
                [bucket_lakenw[3][j].append(tempnw[i][j]) for j in range(0, len(tempnw[i]), 1)]

        if sorted_lakese[6][i] == 0:
            if len(bucket_lakese[0]) == 0:
                to_add = [[j] for j in tempse[i]]
                bucket_lakese[0] = to_add
            else:
                [bucket_lakese[0][j].append(tempse[i][j]) for j in range(0, len(tempse[i]), 1)]
        elif 0 < sorted_lakese[6][i] <= 5:
            if len(bucket_lakese[1]) == 0:
                to_add = [[j] for j in tempse[i]]
                bucket_lakese[1] = to_add
            else:
                [bucket_lakese[1][j].append(tempse[i][j]) for j in range(0, len(tempse[i]), 1)]
        elif 5 < sorted_lakese[6][i] <= 10:
            if len(bucket_lakese[2]) == 0:
                to_add = [[j] for j in tempse[i]]
                bucket_lakese[2] = to_add
            else:
                [bucket_lakese[2][j].append(tempse[i][j]) for j in range(0, len(tempse[i]), 1)]
        else:
            if len(bucket_lakese[3]) == 0:
                to_add = [[j] for j in tempse[i]]
                bucket_lakese[3] = to_add
            else:
                [bucket_lakese[3][j].append(tempse[i][j]) for j in range(0, len(tempse[i]), 1)]

        if sorted_lakesw[6][i] == 0:
            if len(bucket_lakesw[0]) == 0:
                to_add = [[j] for j in tempsw[i]]
                bucket_lakesw[0] = to_add
            else:
                [bucket_lakesw[0][j].append(tempsw[i][j]) for j in range(0, len(tempsw[i]), 1)]
        elif 0 < sorted_lakesw[6][i] <= 5:
            if len(bucket_lakesw[1]) == 0:
                to_add = [[j] for j in tempsw[i]]
                bucket_lakesw[1] = to_add
            else:
                [bucket_lakesw[1][j].append(tempsw[i][j]) for j in range(0, len(tempsw[i]), 1)]
        elif 5 < sorted_lakesw[6][i] <= 10:
            if len(bucket_lakesw[2]) == 0:
                to_add = [[j] for j in tempsw[i]]
                bucket_lakesw[2] = to_add
            else:
                [bucket_lakesw[2][j].append(tempsw[i][j]) for j in range(0, len(tempsw[i]), 1)]
        else:
            if len(bucket_lakesw[3]) == 0:
                to_add = [[j] for j in tempsw[i]]
                bucket_lakesw[3] = to_add
            else:
                [bucket_lakesw[3][j].append(tempsw[i][j]) for j in range(0, len(tempsw[i]), 1)]
    return bucket_laken, bucket_lakenw, bucket_lakese, bucket_lakesw