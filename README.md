# Data Vis - Final Project


## Getting started
Requirements.txt gives the Python version and required packages

## In Order to Run 
Unzip the package and run create_vis.py.  All necessary files for the initial run are
present.  

## Running the data subset
In order to run the data subset, edit line 204 of create_vis.py from 'output.csv' to 'output2.csv'.

