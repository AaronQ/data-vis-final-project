from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.widgets import CheckButtons, Slider, RadioButtons
import matplotlib.dates as mdates
from datetime import datetime


def calc_turb(total_data):
    samples = [[total_data[i][6][j:j+10] for j in range(0, len(total_data[i][6])-10, 1)] for i in range(0, len(total_data), 1)]
    means = [[mean_calc(samples[i][j]) for j in range(0, len(samples[i]), 1)] for i in range(0, len(samples), 1)]
    deviations = [[stan_dev(samples[i][j], means[i][j]) for j in range(0, len(samples[i]), 1)] for i in range(0, len(samples), 1)]
    turb = [[deviations[i][j]/means[i][j] if means[i][j] != 0 else 0 for j in range(0, len(samples[i]), 1)] for i in range(0, len(samples), 1)]
    turb_ave = [round(mean_calc(turb[i]), 2) for i in range(0, len(turb), 1)]
    return turb, turb_ave


def mean_calc(vector):
    """
    determines the mean for list of values
    :param vector: list of the values
    :result mean_value: mean of vector values
    """
    mean_value = float(vector[0])
    for i in range(1, len(vector)):
        mean_value = mean_value + float(vector[i])
    mean_value = round(mean_value / len(vector), 4)
    return mean_value


def stan_dev(vector, mean_value):
  """
  Calculates the standard deviation of the vector data
  :param vector: list of lists containing values
  :param mean_value: mean value of the vector
  :return: stan_dev: list containing the standard deviation values
  """
  stan_dev = 0
  for i in range(0, len(vector)):
    values = float(vector[i]) - mean_value
    values = values ** 2
    stan_dev = stan_dev + values
  stan_dev = (stan_dev / (len(vector) - 1))
  stan_dev = stan_dev ** (1/2)
  return round(stan_dev, 4)


def roundTime(dt=None, roundTo=60):
   """Round a datetime object to any time lapse in seconds
   dt : datetime object, default now.
   roundTo : Closest number of seconds to round to, default 1 minute.
   Author: Thierry Husson 2012 - Use it as you want but don't blame me.
   """
   if dt == None :
       dt = datetime.now()
   seconds = (dt.replace(tzinfo=None) - dt.min).seconds
   rounding = (seconds+roundTo/2) // roundTo * roundTo
   return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)


def create_scatter(fig, labels, sensor_colors, all_buttons, total_data):
    axes = fig.axes
    fig.subplots_adjust(left=.05, bottom=0.035, right=.95, top=0.94, wspace=0, hspace=0)
    scatter_ax = None
    slide_ax = None
    window_ax = None
    turb_ax = None
    wind_gs = None
    for i in range(0, len(axes), 1):
        axes[i].set_visible(not axes[i].get_visible()) if i not in [3, 4, 5, 6, 7, 8, 9] else None
        wind_gs = axes[i].get_gridspec() if i == 1 else wind_gs
        scatter_ax = axes[i] if i == 10 else scatter_ax
        slide_ax = axes[i] if i == 11 else slide_ax
        window_ax = axes[i] if i == 13 else window_ax
        turb_ax = axes[i] if i == 14 else turb_ax
    scatter_ax.set_facecolor('#D9D9D9')
    turb_ax.set_facecolor('#D9D9D9')
    sensor_buttons = all_buttons[1]
    main_button = all_buttons[2]
    # Loop through the four sensors to plot the data
    scatters = []
    turbulences = []
    turb_aves = []
    first = 0
    last = 5760
    time = [datetime.strptime(total_data[0][0][i], '%Y-%m-%d %H:%M:%S') for i, value in enumerate(total_data[0][0])]
    turb, turb_ave = calc_turb(total_data)
    turb_time = time[5:len(time) - 5]
    for i in range(0, len(labels), 1):
        # Plot solar radiation over time and temperature over time.
        scatters.append(scatter_ax.scatter(time[first:last], total_data[i][6][first:last], color=sensor_colors[i]))
        temp, = turb_ax.plot(turb_time[first:last], turb[i][first:last], color=sensor_colors[i])
        turb_aves.append(turb_ax.axhline(turb_ave[i], color=sensor_colors[i], alpha=0.4, visible=False))
        turbulences.append(temp)
    plt.subplots_adjust(hspace=0.25)
    # Create Radio buttons
    radio_button = RadioButtons(window_ax, ('6 hours', '12 hours', '24 hours', '2 days', '4 days'))
    [radio_button.labels[i].set_fontsize('small') for i in range(0, 5, 1)]
    radio_button.set_active(4)
    for buttons in radio_button.circles:
        buttons.width = buttons.width*3/8
    window_ax.set_title('Select window of time:', size='small', loc='left')
    turb_ax.set_title('Turbulence Intensity (10 minute time periods)')
    # Create table showing average turbulence values
    table_ax = fig.add_subplot(wind_gs[20:21, 0:7])
    table_ax.axis('off')
    table_ax.table(cellText=list(zip(*[labels, turb_ave])), colLabels=['Sensor', 'Average Turbulence'], cellLoc='center')
    # Create checkbox to show turbulence averages on plot
    ave_plts_ax = fig.add_subplot(wind_gs[25:27, 0:6])
    turb_ave_button = CheckButtons(ave_plts_ax, ['Show averages on turbulence plot'])
    for boxes in turb_ave_button.rectangles:
        boxes.set_height(boxes.get_width()*7/3)
    turb_ave_button.labels[0].set_fontsize('small')

    def time_change(value):
        """
        Changes the data of the plot to the time period shown
        :param value: Slidervalue
        """
        # Get interval range
        lower = int(value - (last-first)/2)
        upper = int(value + (last-first)/2)
        time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
        time_slider.valmin = ((last - first) / 2)
        visibility = selected_sensors if any(selected_sensors) else [not selected_sensors[i] for i in range(0, len(selected_sensors), 1)]
        for i in range(0, 4, 1):
            scatters[i].remove()
            scatters[i] = scatter_ax.scatter(time[lower:upper], total_data[i][6][lower:upper], color=sensor_colors[i],
                                             visible=visibility[i])
            turbulences[i].remove()
            turbulences[i], = turb_ax.plot(turb_time[lower:upper], turb[i][lower:upper], color=sensor_colors[i],
                                             visible=visibility[i])
        scatter_ax.relim()
        turb_ax.relim()
        turb_ax.autoscale_view()
        turb_ax.autoscale()
        scatter_ax.set_xlim(turb_ax.get_xlim())
        scatter_ax.autoscale_view()
        time_slider.valtext.set_text(datetime.strftime(time[lower], '%m-%d %H:%M') + ' to \n' +
                                    datetime.strftime(time[upper], '%m-%d %H:%M'))
        time_dif_sec = time[upper] - time[lower]
        time_dif = time_dif_sec.days * 24 + time_dif_sec.seconds / 3600
        if time_dif <= 6:
            time_int = 30
            scatter_ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=time_int))
            turb_ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=time_int))
            fig.canvas.draw()
            return
        elif time_dif <= 12:
            time_int = 1
        elif time_dif <= 24:
            time_int = 2
        elif time_dif <= 48:
            time_int = 4
        else:
            time_int = 8
        scatter_ax.xaxis.set_major_locator(mdates.HourLocator(interval=time_int))
        turb_ax.xaxis.set_major_locator(mdates.HourLocator(interval=time_int))
        fig.canvas.draw()
        return

    def int_select(label):
        """
        Changes the size of the interval based on the selected radiobuttons
        :param label: label of the radiobutton
        """
        nonlocal first, last
        if label == '6 hours':
            first = 0
            last = 360
            temp_val = 180
            time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
            time_slider.valmin = ((last - first) / 2)
        elif label == '12 hours':
            first = 0
            last = 720
            temp_val = 360
            time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
            time_slider.valmin = ((last - first) / 2)
        elif label == '24 hours':
            first = 0
            last = 1440
            temp_val = 720
            time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
            time_slider.valmin = ((last - first) / 2)
        elif label == '2 days':
            first = 0
            last = 2880
            temp_val = 1440
            time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
            time_slider.valmin = ((last - first) / 2)
        else:
            first = 0
            last = 5760
            temp_val = 2880
            time_slider.valmax = (len(time) - 1 - ((last - first) / 2))
            time_slider.valmin = ((last - first) / 2)
        if all([time_slider.val != 180, time_slider.val != 360, time_slider.val != 720, time_slider.val != 1440,
                time_slider.val != 2880]):
            temp_val = time_slider.val
        else:
            time_slider.set_val(temp_val)
        time_change(temp_val)
        return


    # Create rangeslider for selecting span of plot range
    time_slider = Slider(slide_ax, 'Minutes ', 0, len(time) - 1-((last-first)/2),
                         valinit=((last-first)/2), valfmt='%-G', valstep=1)
    time_slider.valtext.set_text(datetime.strftime(time[first], '%m-%d %H:%M') + ' to \n' +
                                 datetime.strftime(time[last], '%m-%d %H:%M'))
    time_slider.valtext.set(wrap=True)
    time_slider.vline._linewidth=0

    legend_lines = []
    for i in range(0, len(sensor_colors), 1):
        legend_lines.append(Line2D([0], [0], color=sensor_colors[i], lw=4))
    # Set the titles and axis labels of the plots
    scatter_ax.set_title('Wind Speed vs Time')
    scatter_ax.set_ylabel('Miles Per Hour (MPH)')
    # Modify the plot axis and tick marks
    plt.draw()
    scatter_ax.set_xticks(scatter_ax.get_xticks())
    turb_ax.set_xticks(turb_ax.get_xticks())
    scatter_ax.xaxis.set_major_locator(mdates.HourLocator(interval=8))
    turb_ax.xaxis.set_major_locator(mdates.HourLocator(interval=8))
    scatter_ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    turb_ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    scatter_ax.legend(legend_lines, labels, fontsize='small', loc='upper right', bbox_to_anchor=(0.9975, 0.96))

    # Create array of selected sensors
    selected_sensors = [False, False, False, False]

    def sensor_clicked(event, index):
        """Hide or Show sensor plots
        index: corresponding sensor index
        leg: figure legend object
        return: leg
        """
        selected_sensors[index] = not selected_sensors[index]
        # Change color of selected button
        if selected_sensors[index]:
            sensor_buttons[index].color = 'gray'
            sensor_buttons[index].label.set_color('black')
        else:
            sensor_buttons[index].color = 'white'
            sensor_buttons[index].label.set_color(sensor_colors[index])
        # Change visibility of plots
        if all(selected_sensors) or not any(selected_sensors):
            for i in range(0, 4, 1):
                sensor_buttons[i].color = 'white'
                sensor_buttons[i].label.set_color(sensor_colors[i])
                selected_sensors[i] = False
                scatters[i].set_visible(True)
                turbulences[i].set_visible(True)
                turb_aves[i].set_visible(True) if turb_ave_button.get_status()[0] else None
        else:
            for i in range(0, 4, 1):
                if not selected_sensors[i]:
                    scatters[i].set_visible(False)
                    turbulences[i].set_visible(False)
                    turb_aves[i].set_visible(False)
                else:
                    scatters[i].set_visible(True)
                    turbulences[i].set_visible(True)
                    turb_aves[i].set_visible(True) if turb_ave_button.get_status()[0] else None

    def show_aves(label):
        for ave in turb_aves:
            ave.set_visible(turb_ave_button.get_status()[0])
        fig.canvas.draw()
        return

    def main_clicked(event):
        table_ax.set_visible(False)
        ave_plts_ax.set_visible(False)
        for i in range(0, len(axes), 1):
            if i in [10, 11, 13, 14]:
                axes[i].set_visible(False) if axes[i].get_visible() else None
            else:
                axes[i].set_visible(not axes[i].get_visible()) if i not in [3, 4, 5, 6, 7, 8, 9] else None
        fig.subplots_adjust(left=0, bottom=0.05, top=1, right=1, wspace=1, hspace=1)
        # disconnect button events
        main_button.disconnect(mb_cid)
        radio_button.disconnect(rb_cid)
        turb_ave_button.disconnect(ta_cid)
        time_slider.disconnect(ts_cid)
        [sensor_buttons[i].disconnect(sb_cid[i]) for i in range(0, len(sensor_buttons), 1)]
        if any(selected_sensors):
            [sensor_clicked(None, i) for i in range(0, len(selected_sensors), 1) if selected_sensors[i]]
        return


    mb_cid = main_button.on_clicked(main_clicked)
    rb_cid = radio_button.on_clicked(int_select)
    ta_cid = turb_ave_button.on_clicked(show_aves)
    sb_cid = [sensor_buttons[0].on_clicked(lambda x: sensor_clicked(x, 0))]
    sb_cid.append(sensor_buttons[1].on_clicked(lambda x: sensor_clicked(x, 1)))
    sb_cid.append(sensor_buttons[2].on_clicked(lambda x: sensor_clicked(x, 2)))
    sb_cid.append(sensor_buttons[3].on_clicked(lambda x: sensor_clicked(x, 3)))
    ts_cid = time_slider.on_changed(time_change)
    plt.show()
