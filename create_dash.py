from matplotlib import pyplot as plt
from matplotlib.widgets import RadioButtons, Button, CheckButtons
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition


def create_map(sensor_colors, labels, gen_dir_types):
    """
    This function takes the sensor_colors and labels to construct the dashboard.  The dashboard shows a map of the 
    property with the location of the existing sensor
    :param sensor_colors: string list giving the colors for each of the sensors
    :param labels: string list giving the names of the sensors
    :return: fig: figure object of the dashboard
    """
    # read map images and create figure with gridspec
    main_img = plt.imread("main_map2.png")
    sub_img = plt.imread("sub_map.png")
    fig = plt.figure(figsize=(15, 8))
    map_gs = fig.add_gridspec(30, 30)
    # Create initial subplots and show map
    main_map = fig.add_subplot(map_gs[0:26, 0:31])
    select_ax = fig.add_subplot(map_gs[27:31, 7:11])
    main_map.imshow(main_img, interpolation='nearest')
    main_map.axis('off')
    # Create hidden subplots for use in the graphs
    sub_map = fig.add_subplot(map_gs[0:16, 0:8], visible=False)
    cbax = fig.add_subplot(map_gs[17:20, 0:6], visible=False)
    wdax = fig.add_subplot(map_gs[22:27, 0:4], visible=False)
    wr_plots = [fig.add_subplot(map_gs[0:13, 10:20], projection="windrose", visible=False),
                fig.add_subplot(map_gs[0:13, 21:31], projection="windrose", visible=False),
                fig.add_subplot(map_gs[17:30, 10:20], projection="windrose", visible=False),
                fig.add_subplot(map_gs[17:30, 21:31], projection="windrose", visible=False)]
    box_plots = fig.add_subplot(map_gs[0:29, 11:31], visible=False)
    scatter_ax = fig.add_subplot(map_gs[0:20, 11:31], visible=False)
    slide_ax = fig.add_subplot(map_gs[29, 11:28], visible=False)
    slide_ax.axis('off')
    mb_loc = fig.add_subplot(map_gs[29, 0:2], visible=False)
    window_ax = fig.add_subplot(map_gs[16:20, 0:5], visible=False)
    turb_ax = fig.add_subplot(map_gs[22:27, 11:31], visible=False)
    # Adjust figure parameters
    fig.subplots_adjust(left=0, bottom=0, top=1, right=1, wspace=1, hspace=1)
    sub_map.imshow(sub_img, interpolation='nearest')

    cbax.set_title('Select wind speeds to show:', loc='left', size='small')
    wind_labels = ['All (default)', 'No Wind', 'Low Wind < 5 mph',
                   'Medium Wind $\geq$ 5 mph < 10 mph', 'High Wind $\geq$ 10 mph']
    check_boxes = CheckButtons(cbax, wind_labels)
    check_boxes.set_active(0)
    [check_boxes.labels[i].set_fontsize('small') for i in range(0, len(wind_labels), 1)]
    for boxes in check_boxes.rectangles:
        boxes.set_height(boxes.get_width()*5/3)
    gen_dir_labels = ['All (Default)']
    [gen_dir_labels.append(gen_dir_types[i]) for i in range(0, len(gen_dir_types), 1)]
    wind_dir_buttons = RadioButtons(wdax, gen_dir_labels)
    temp = wdax.get_position()
    radius = temp.height
    for buttons in wind_dir_buttons.circles:
        buttons.width = radius*3/8

    # Specify sensor locations and labels
    sub_x = [554, 648, 315, 555]
    sub_y = [299, 205, 705, 850]

    # Plot sensors on the sub map and annotate with buttons
    sub_map.scatter(sub_x, sub_y, color=sensor_colors)

    # Create Sensor Filter buttons
    # Set position of the buttons
    nw_loc = plt.axes([0, 0, 1, 1], visible=False)
    nw_ip = InsetPosition(sub_map.axes, [0.4925, 0.658, .27, 0.06])
    nw_loc.set_axes_locator(nw_ip)
    n_loc = plt.axes([0, 0, 1, 1], visible=False)
    n_ip = InsetPosition(sub_map.axes, [0.575, 0.74, .26, 0.06])
    n_loc.set_axes_locator(n_ip)
    sw_loc = plt.axes([0, 0, 1, 1], visible=False)
    sw_ip = InsetPosition(sub_map.axes, [0.285, 0.28, .27, 0.06])
    sw_loc.set_axes_locator(sw_ip)
    se_loc = plt.axes([0, 0, 1, 1], visible=False)
    se_ip = InsetPosition(sub_map.axes, [0.495, 0.15, .27, 0.06])
    se_loc.set_axes_locator(se_ip)

    # Create buttons, list of buttons and change font
    sensor_buttons = [Button(nw_loc, 'LakeNW Sensor', color='white')]
    sensor_buttons.append(Button(n_loc, 'LakeN Sensor', color='white'))
    sensor_buttons.append(Button(sw_loc, 'LakeSW Sensor', color='white'))
    sensor_buttons.append(Button(se_loc, 'LakeSE Sensor', color='white'))
    # Create button to return to main page
    main_button = Button(mb_loc, 'Main View')

    sensor_buttons[0].label.set_horizontalalignment('center')
    sensor_buttons[0].label.set_verticalalignment('center')
    sensor_buttons[0].label.set_verticalalignment('center')
    sensor_buttons[0].label.set_color(sensor_colors[0])
    sensor_buttons[0].label.set_fontsize('small')
    sensor_buttons[1].label.set_color(sensor_colors[1])
    sensor_buttons[1].label.set_fontsize('small')
    sensor_buttons[2].label.set_color(sensor_colors[2])
    sensor_buttons[2].label.set_fontsize('small')
    sensor_buttons[3].label.set_color(sensor_colors[3])
    sensor_buttons[3].label.set_fontsize('small')
    sub_map.axis('off')

    # Create Radio Buttons
    select_ax.set_title('Choose graph to show:', loc='left', size='small')
    radio_buttons = RadioButtons(select_ax, ('Wind Rose', 'Box and Whisker', 'Scatter'),
                                 activecolor='white')
    [radio_buttons.labels[i].set_fontsize('small') for i in range(0, 3, 1)]
    temp = select_ax.get_position()
    radius = temp.height
    x1, y2 = select_ax.get_position() * fig.get_size_inches()
    width, height = y2-x1
    for buttons in radio_buttons.circles:
        buttons.width = radius * height/width * 2
        buttons.height = radius * 2


    # Combine all buttons into one list
    all_buttons = [radio_buttons, sensor_buttons, main_button, check_boxes, wind_dir_buttons]
    return fig, all_buttons, wind_labels
