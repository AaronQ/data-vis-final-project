from ambient_api.ambientapi import AmbientAPI
import os
import bitdotio
import pandas as pd
import numpy as np

sensor_names = []
db = None


def pull_data(sensor_names, db):
    """Pull sensor GPS locations and sensor data as panda dataframe.  Combine all data into a panda dataframe
    :param sensor_names: list of the sensor names
    :param db: bit.io api connection
    :return: raw_data: pandas dataframe containing the raw data"""

    # Pull GPS locations
    conn = db.get_connection('Psypoch/data')
    sql = f'''Select * FROM Sensor_Loc'''
    sensor_loc = pd.read_sql(sql, conn)
    # Create new dataframe for combined output.
    total_data = pd.DataFrame()
    # Loop through sensor names and pull data for each of them.
    for sensor in sensor_names:
        sensor_location_data = sensor_loc.loc[sensor_loc['sensor_name'] == sensor]
        sql = f'''Select * FROM "{sensor}"'''
        sensor_data = pd.read_sql(sql, conn)
        # Insert the sensor name, latitude, and longitude of each sensor
        sensor_data.insert(0, sensor_loc.columns[0],
                           sensor_loc[sensor_loc.columns[0]].iloc[sensor_loc[sensor_loc[sensor_loc.columns[0]] == sensor].index[0]])
        sensor_data.insert(1, sensor_loc.columns[1],
                           sensor_loc[sensor_loc.columns[1]].iloc[sensor_loc[sensor_loc[sensor_loc.columns[0]] == sensor].index[0]])
        sensor_data.insert(2, sensor_loc.columns[2],
                           sensor_loc[sensor_loc.columns[2]].iloc[sensor_loc[sensor_loc[sensor_loc.columns[0]] == sensor].index[0]])
        total_data = total_data.append(sensor_data)
    total_data.set_index('date_time', inplace=True)
    total_data.sort_index()
    conn.close()
    return total_data


def clean_data(raw_data):
    """
    This function takes the combined raw data from all the sensors and removes the row of data for a datetime that
    does not have a corresponding datetime for all other sensors
    :param raw_data: dataframe of all the sensors data
    :return: cleaned_data: dataframe with the data cleaned from all the sensors
    """
    # Convert datetime integers to datetime object
    date_values = raw_data.index.values
    to_drop = []
    for date_value in date_values:
        dt_count = np.count_nonzero(date_values == date_value)
        if dt_count != 4:
            to_drop.append(date_value)
    to_drop = list(set(to_drop))
    raw_data.drop(to_drop, inplace=True)
    raw_data.index = pd.to_datetime(raw_data.index, unit='ms')
    return raw_data

if __name__ == '__main__':
    """
    This script reads the environment variables, connects to the wind sensors to pull the name of the sensors, pulls the
    sensor data stored in the bit.io database, cleans the data, and then writes the data to a csv file.
    """
    # initialize variables from environment
    endpoint = os.environ["AMBIENT_ENDPOINT"]
    rt_endpoint = os.environ["AMBIENT_RT_ENDPOINT"]
    app_key = os.environ["AMBIENT_APPLICATION_KEY"]
    api_key = os.environ["AMBIENT_API_KEY"]
    database_api_key = os.environ['BIT_IO_API_Key']
    db = bitdotio.bitdotio(database_api_key)
    api = AmbientAPI()
    devices = api.get_devices()
    # Loop through the devices and get the names of the sensors.
    for device in devices:
        sensor_names.append(device.info.get('name').rstrip())

    # Pull data into dataframe
    raw_data = pull_data(sensor_names, db)
    cleaned_data = clean_data(raw_data)

    # Write out data
    cleaned_data.to_csv('output.csv', index=True, mode='w')


